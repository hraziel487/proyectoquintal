import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  rutas = [
    {
name:'Inicio',
path:'/Inicio'
    },
    {
   name:'formulario',
   path:'/formulario'
    }, 
     {
      name:'Api',
      path:'/posts'
    }
  ];



  constructor() { }

  ngOnInit(): void {
  }

}
