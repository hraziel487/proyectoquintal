import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { FormularioComponent } from './formulario/formulario.component';
import { ApiComponent } from './api/api.component';



@NgModule({
  declarations: [
    InicioComponent,
    FormularioComponent,
    ApiComponent
  ],
  exports:[
    InicioComponent,
    FormularioComponent,
    ApiComponent
  ],
  imports:[
    CommonModule
  ]
})
export class PagesModule { }
